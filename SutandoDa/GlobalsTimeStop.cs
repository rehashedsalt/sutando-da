﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.DataStructures;

// Most of this was taken from Antiaris
// However, the entire player section, along with a few extras, are original
// Thanks for the foundation, zadum! I wouldn't've known where to begin without your
// Time Paradox Crystal and related code. In fact, I'd go as far as to say that your
// mod gave me the confidence to actually move forward with learning tModLoader.

// Other parts of this mod that were copied or outright lifted from Antiaris include:
// * BuffTimeStop
// * TimeSky

// I also did fix a few bugs with it, including enemies' tendencies to not damage you after
// you stop time, and changed the implementation of Item freezing.

namespace SutandoDa
{
    class ProjectileStop : GlobalProjectile
    {
        private int ProjDamage;
        public int Tick;
        public override bool InstancePerEntity { get { return true; } }

        private static readonly int[] badAIs = new[] {
            7, // Grappling hook
            13, // Harpoon
            19, // Spears/Polearms
            20, // Drills/Chainsaws
            75, // Confetti Grenade?
            99, // Boulders
            140, // Rocket IIIs?
            142, // Grenade IVs?
        };

        public override bool PreAI(Projectile projectile)
        {
            Player player = Main.player[Main.myPlayer];
            Tick++;
            if (projectile.damage != 0) ProjDamage = projectile.damage;
            if (SutandoDaWorld.timeStopped > 0 && Tick >=4 && !projectile.minion && !badAIs.Contains(projectile.aiStyle))
            {
                projectile.timeLeft++;
                if (!projectile.friendly && projectile.aiStyle != 113)
                {
                    projectile.damage = 0;
                }
                return false;
            }
            else
            {
                if (!projectile.friendly && projectile.damage != ProjDamage && !badAIs.Contains(projectile.aiStyle))
                {
                    projectile.damage = ProjDamage;
                }
                return true;
            }
        }

        public override bool ShouldUpdatePosition(Projectile projectile)
        {
            Player player = Main.player[Main.myPlayer];
            if (SutandoDaWorld.timeStopped > 0 && !projectile.minion && !badAIs.Contains(projectile.aiStyle))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    
    public class ItemStop : GlobalItem
    {
        private bool timeStopVelocityApplied = true;
        private Vector2 timeStopVelocity = new Vector2(0, 0);
        private bool timeStopPositionStored = false;
        private Vector2 timeStopPosition = new Vector2(0,0);

        public override bool InstancePerEntity { get { return true; } }
        public override bool CloneNewInstances { get { return true; } }
        
        public override void Update(Item item, ref float gravity, ref float maxFallSpeed)
        {
            // Position
            if (SutandoDaWorld.timeStopped > 0 && timeStopPositionStored)
            {
                item.position = timeStopPosition;
            }
            else
            {
                timeStopPosition = item.position;
                timeStopPositionStored = true;
            }
            // Velocity
            if (SutandoDaWorld.timeStopped > 0)
            {
                timeStopVelocityApplied = false;
                item.velocity = new Vector2(0, 0);
            }
            else if (!timeStopVelocityApplied)
            {
                timeStopVelocityApplied = true;
                item.velocity += timeStopVelocity;
            }
            else
            {
                timeStopVelocity = item.velocity;
            }
        }
    }
    
    public class NPCStop : GlobalNPC
    {
        public override bool InstancePerEntity { get { return true; } }
        private Rectangle timeStopFrame;

        private int timeStopBuildupDamage;
        private float timeStopBuildupKnockback;
        private int timeStopBuildupHitDirection;

        private int timeStopOrigDamage = 0;
        private double timeStopOrigFrameCounter = 0;

        public override void SetDefaults(NPC npc)
        {
            if (SutandoDaWorld.timeStopped > 0)
            {
                npc.frame = this.timeStopFrame;
            }
            else
            {
                timeStopFrame = npc.frame;
            }
        }

        public override bool PreAI(NPC npc)
        {
            if (SutandoDaWorld.timeStopped > 0)
            {
                npc.position.X = npc.oldPosition.X;
                npc.position.Y = npc.oldPosition.Y;
                if (timeStopOrigFrameCounter == 0) timeStopOrigFrameCounter = npc.frameCounter;
                npc.frameCounter = 0;
                if (timeStopOrigDamage == 0) timeStopOrigDamage = npc.damage;
                npc.damage = 0;
                return false;
            }
            else
            {
                if (timeStopOrigDamage > 0)
                {
                    npc.damage = timeStopOrigDamage;
                    timeStopOrigDamage = 0;
                }
                if (timeStopOrigFrameCounter != 0)
                {
                    npc.frameCounter = timeStopOrigFrameCounter;
                    timeStopOrigFrameCounter = 0d;
                }
                if (timeStopBuildupDamage > 0)
                {
                    npc.StrikeNPC(timeStopBuildupDamage, timeStopBuildupKnockback, timeStopBuildupHitDirection);
                    timeStopBuildupDamage = 0;
                    timeStopBuildupKnockback = 0;
                }
                return true;
            }
        }

        public override void ModifyHitByItem(NPC npc, Player player, Item item, ref int damage, ref float knockback, ref bool crit)
        {
            if (SutandoDaWorld.timeStopped > 0)
            {
                if (crit)
                {
                    timeStopBuildupDamage += damage * 2;
                }
                else timeStopBuildupDamage += damage / 2;
                timeStopBuildupKnockback += knockback;
                timeStopBuildupHitDirection = player.direction;
                damage = 0;
                crit = false;
                knockback = 0;
                npc.life++;
            }
            else base.ModifyHitByItem(npc, player, item, ref damage, ref knockback, ref crit);
        }

        public override void ModifyHitByProjectile(NPC npc, Projectile projectile, ref int damage, ref float knockback, ref bool crit, ref int hitDirection)
        {
            if (SutandoDaWorld.timeStopped > 0)
            {
                if (crit)
                {
                    timeStopBuildupDamage += damage * 2;
                }
                else timeStopBuildupDamage += damage / 2;
                timeStopBuildupKnockback += knockback;
                timeStopBuildupHitDirection = hitDirection;
                damage = 0;
                crit = false;
                knockback = 0;
                npc.life++;
            }
            else base.ModifyHitByProjectile(npc, projectile, ref damage, ref knockback, ref crit, ref hitDirection);
        }
    }

    public class TileAnimStop : GlobalTile
    {
        public override void AnimateTile()
        {
            if (SutandoDaWorld.timeStopped > 0)
            {
                for(int i = 0; i < 469; i++)
                {
                    Main.tileFrameCounter[i] = 0;
                }
            }
        }
    }

    public class PlayerStop : ModPlayer
    {
        public bool timeStopImmune = false;
        public bool timeStopVelocityApplied = true;
        public Vector2 timeStopVelocity = new Vector2(0, 0);

        public int timeStopBuildupDamage = 0;
        public bool timeStopBuildupPvp = false;
        public int timeStopBuildupHitDirection;

        public override void PreUpdate()
        {
            if (timeStopBuildupDamage > 0 && !(SutandoDaWorld.timeStopped > 0))
            {
                PlayerDeathReason reason = PlayerDeathReason.ByCustomReason(player.name + " just suddenly wasn't");
                if (timeStopBuildupPvp)
                {
                    switch (Main.rand.Next(4))
                    {
                        case 0:
                            reason = PlayerDeathReason.ByCustomReason(player.name + "'s Emerald Splash belongs in the trash");
                            break;
                        case 1:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " had too much confidence in their Hierophant Green");
                            break;
                        case 2:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " was turned into a donut");
                            break;
                        case 3:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " shot the clock tower on their way out");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (Main.rand.Next(4))
                    {
                        case 0:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " fell down the stairs while time was stopped");
                            break;
                        case 1:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " proved that stopping time doesn't make you immortal");
                            break;
                        case 2:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " gained a few holes when time caught back up");
                            break;
                        case 3:
                            reason = PlayerDeathReason.ByCustomReason(player.name + " lost a few limbs when time caught back up");
                            break;
                        default:
                            break;
                    }
                }
                player.Hurt(reason, timeStopBuildupDamage, timeStopBuildupHitDirection);
            }
        }

        public override void PreUpdateMovement()
        {
            if (SutandoDaWorld.timeStopped > 0 && !timeStopImmune)
            {
                timeStopVelocityApplied = false;
                player.velocity = new Vector2(0,0);
            }
            else if (!timeStopVelocityApplied)
            {
                timeStopVelocityApplied = true;
                player.velocity += timeStopVelocity;
            }
            else
            {
                timeStopVelocity = player.velocity;
            }
        }

        public override bool PreItemCheck()
        {
            int[] usableItems =
            {
                mod.GetItem("ItemStarPlatinum").item.whoAmI,
                mod.GetItem("ItemTheWorld").item.whoAmI
            };
            if (usableItems.Contains(player.HeldItem.whoAmI))
            {
                return true;
            }
            else return !(SutandoDaWorld.timeStopped > 0 && !timeStopImmune);
        }

        public override bool PreHurt(bool pvp, bool quiet, ref int damage, ref int hitDirection, ref bool crit, ref bool customDamage, ref bool playSound, ref bool genGore, ref PlayerDeathReason damageSource)
        {
            if (SutandoDaWorld.timeStopped > 0 && !timeStopImmune)
            {
                if (damage >= 0)
                {
                    if (crit)
                    {
                        timeStopBuildupDamage += damage * 2;
                    }
                    else
                    {
                        timeStopBuildupDamage += damage;
                    }
                    if (pvp)
                    {
                        timeStopBuildupPvp = true;
                    }
                    timeStopBuildupHitDirection = hitDirection;
                }
                return false;
            }
            else
            {
                timeStopBuildupDamage = 0;
                return true;
            }
        }
    }
}
