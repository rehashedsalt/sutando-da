﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Dusts
{
    public class DustStandPowerGlow : ModDust
    {
        public override void OnSpawn(Dust dust)
        {
            dust.noGravity = true;
            dust.frame = new Rectangle(0, 0, 32, 10);
            dust.alpha = 240;
            //dust.scale = 0;
        }

        public override bool Update(Dust dust)
        {
            dust.position += dust.velocity;
            if (dust.scale < 1 && dust.alpha < 1)
            {
                dust.scale += 0.5f;
            }
            else
            {
                dust.alpha += 2;
                //dust.scale -= 0.1f;
            }
            if (dust.alpha >= 255)
            {
                dust.active = false;
            }
            // Obtained these magical RGB values by color-sampling the sprite
            //Vector3 color = new Vector3(0.864f, 0.676f, 0.046f);
            //Lighting.AddLight(dust.position, Vector3.Multiply(color, 0.1f));
            return false;
        }
    }
}
