﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Dusts
{
    public class DustMenacing : ModDust
    {
        public override void OnSpawn(Dust dust)
        {
            dust.noGravity = true;
            dust.velocity = new Vector2(Main.rand.NextFloat(-0.2f, 0.2f), -1.5f);
            dust.frame = new Rectangle(0, 0, 32, 32);
        }

        public override bool Update(Dust dust)
        {
            dust.position += Vector2.Multiply(dust.velocity, Main.rand.Next(0, 2));
            if (Main.time % 10 == 0)
            {
                if (dust.scale >= 1f)
                {
                    dust.scale *= Main.rand.NextFloat(0.75f, 0.8f);
                }
                else
                {
                    dust.scale *= Main.rand.NextFloat(1.1f, 1.3f);
                }
                dust.alpha += 7;
            }
            if (dust.alpha >= 255)
            {
                dust.active = false;
            }
            // Obtained these magical RGB values by color-sampling the sprite
            Lighting.AddLight(dust.position, 0.239f, 0.122f, 0.349f);
            return false;
        }
    }
}
