﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Dusts
{
    public class DustHitHardDark : ModDust
    {
        public override void OnSpawn(Dust dust)
        {
            dust.noGravity = true;
            dust.velocity = new Vector2(Main.rand.NextFloat(-0.2f, 0.2f), -1.5f);
            dust.frame = new Rectangle(0, 0, 32, 32);
        }

        public override bool Update(Dust dust)
        {
            if (dust.alpha < 12)
            {
                if (Main.time % 3 == 0)
                {
                    dust.alpha++;
                    if (dust.scale >= 1.4f)
                    {
                        dust.scale *= Main.rand.NextFloat(0.75f, 0.8f);
                    }
                    else
                    {
                        dust.scale *= Main.rand.NextFloat(1.1f, 1.3f);
                    }
                }
            }
            else
            {
                dust.alpha += 20;
            }
            if (dust.alpha >= 255)
            {
                dust.active = false;
            }
            // Obtained these magical RGB values by color-sampling the sprite
            Lighting.AddLight(dust.position, 0.949f, 0.384f, 0.047f);
            return false;
        }
    }
}
