﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Dusts
{
    public class DustKillerQueenEvil : ModDust
    {
        public override void OnSpawn(Dust dust)
        {
            dust.noGravity = true;
            dust.frame = new Rectangle(0, 0, 5, 5);
        }

        public override bool Update(Dust dust)
        {
            dust.position += Vector2.Multiply(dust.velocity, Main.rand.NextFloat(-2f, 2f));
            dust.scale -= 0.05f;
            if (dust.scale <= 0.05f)
            {
                dust.active = false;
            }
            // Menacing light
            Lighting.AddLight(dust.position, 0.239f, 0.122f, 0.349f);
            return false;
        }
    }
}
