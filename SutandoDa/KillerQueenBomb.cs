﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.DataStructures;
using Terraria.ModLoader;

namespace SutandoDa
{
    /// <summary>
    /// Killer Queen's primary bomb effect
    /// </summary>
    public class KillerQueenBomb
    {
        private Mod mod;
        private Player owner;
        private Player targetPlayer = null;
        private NPC targetNpc = null;
        private Vector2 targetCoords = new Vector2(0,0);
        public float triggerDistance = 48f;

        public KillerQueenBomb(Mod mod, Player owner, Player target)
        {
            this.owner = owner;
            this.mod = mod;
            targetPlayer = target;
            if (Main.player[Main.myPlayer] == owner) Main.NewText("Bomb set on player " + targetPlayer.name, 255, 255, 128);
        }

        public KillerQueenBomb(Mod mod, Player owner, NPC npc)
        {
            this.owner = owner;
            this.mod = mod;
            targetNpc = npc;
            if (Main.player[Main.myPlayer] == owner) Main.NewText("Bomb set on " + targetNpc.FullName, 255, 128, 128);
        }

        public KillerQueenBomb(Mod mod, Player owner, Vector2 coordsInWorld)
        {
            this.owner = owner;
            this.mod = mod;
            targetCoords = coordsInWorld;
            if (Main.player[Main.myPlayer] == owner) Main.NewText("Bomb set at x" + targetCoords.X + " y" + targetCoords.Y, 128, 128, 128);
        }

        public void Validate()
        {
            if (targetPlayer != null && (!Main.player.Contains(targetPlayer) || targetPlayer.statLife < 1)) Remove();
            if (targetNpc != null && (!Main.npc.Contains(targetNpc) || targetNpc.life < 1)) Remove();
        }

        /// <summary>
        /// Detonate Killer Queen's primary bomb
        /// </summary>
        public void Detonate()
        {
            if (targetPlayer != null)
            {
                PlayerDeathReason reason;
                switch (Main.rand.Next(5))
                {
                    case 1:
                        List<string> validItemNames = new List<string>();
                        string chosenItemName = "school badge";
                        foreach (Item i in targetPlayer.inventory)
                        {
                            if (i != null && i.Name != null && i.Name != "")
                            {
                                validItemNames.Add(i.Name);
                            }
                        }
                        if (validItemNames.Count > 0)
                        {
                            chosenItemName = validItemNames.ElementAt(Main.rand.Next(0, validItemNames.Count));
                        }
                        reason = PlayerDeathReason.ByCustomReason("Killer Queen already touched " + targetPlayer.name + "'s " + chosenItemName);
                        break;
                    case 2:
                        reason = PlayerDeathReason.ByCustomReason(targetPlayer.name + " reached for the wrong doorknob");
                        break;
                    case 3:
                        reason = PlayerDeathReason.ByCustomReason(targetPlayer.name + " vanished in a cloud of smoke");
                        break;
                    case 4:
                        reason = PlayerDeathReason.ByCustomReason(targetPlayer.name + " isn't a big fan of Queen");
                        break;
                    default:
                        reason = PlayerDeathReason.ByCustomReason(targetPlayer.name + " picked up an innocuous 100 yen coin");
                        break;
                }
                targetPlayer.Hurt(reason, int.MaxValue, 0, true, false, true);
                if (Main.player[Main.myPlayer] == owner || Main.player[Main.myPlayer] == targetPlayer)
                {
                    Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenDeploy"), targetPlayer.position);
                }
                if (Main.player[Main.myPlayer] == owner) Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), owner.position);
                Projectile.NewProjectile(targetPlayer.position, new Vector2(0, 0), mod.ProjectileType("ExplosionKillerQueen"), 150, 16, owner.whoAmI);
                owner.AddBuff(mod.BuffType("DebuffBombCooldown"), 300);
                Remove();
            }
            else if (targetNpc != null)
            {
                if (Main.player[Main.myPlayer] == owner)
                {
                    Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenDeploy"), targetNpc.position);
                }
                if (targetNpc.boss)
                {
                    targetNpc.StrikeNPC(targetNpc.lifeMax / 20, 0, 0, false);
                    owner.AddBuff(mod.BuffType("DebuffBombCooldown"), 1800);
                }
                else
                {
                    targetNpc.StrikeNPC(16384 + Main.rand.Next(-8192, 8192), 0, 0, true, false);
                    owner.AddBuff(mod.BuffType("DebuffBombCooldown"), 120);
                }
                if (Main.player[Main.myPlayer] == owner) Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), owner.position);
                Projectile.NewProjectile(targetNpc.position, new Vector2(0,0), mod.ProjectileType("ExplosionKillerQueen"), 150, 16, owner.whoAmI);
                Remove();
            }
            else
            {
                if (Main.player[Main.myPlayer] == owner)
                {
                    Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenDeploy"), targetCoords);
                    Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), owner.position);
                }
                Projectile.NewProjectile(targetCoords, new Vector2(0, 0), mod.ProjectileType("ExplosionKillerQueen"), 100, 16, owner.whoAmI);
                owner.AddBuff(mod.BuffType("DebuffBombCooldown"), 120);
                Remove();
            }
        }

        public bool TryAutoDetonate()
        {
            foreach (Player p in Main.player)
            {
                if (Vector2.Distance(p.position, Position()) <= triggerDistance && p.whoAmI != owner.whoAmI && (targetPlayer == null || targetPlayer.whoAmI != p.whoAmI) && p.team != owner.team && p.pvpDeath)
                {
                    Detonate();
                    return true;
                }
            }
            foreach (NPC n in Main.npc)
            {
                if (Vector2.Distance(n.position, Position()) <= triggerDistance && (targetNpc == null || targetNpc.whoAmI != n.whoAmI) && !n.immortal && !n.friendly && n.life > 0)
                {
                    Detonate();
                    return true;
                }
            }
            return false;
        }

        public Vector2 Position()
        {
            if (targetPlayer != null)
            {
                return new Vector2(targetPlayer.position.X + targetPlayer.width / 2, targetPlayer.position.Y + targetPlayer.height/2);
            }
            else if (targetNpc != null)
            {
                return new Vector2(targetNpc.position.X + targetNpc.width / 2, targetNpc.position.Y + targetNpc.height / 2);
            }
            else return targetCoords;
        }

        private void Remove()
        {
            SutandoDaPlayer sPlayer = owner.GetModPlayer<SutandoDaPlayer>();
            sPlayer.killerQueenBomb = null;
        }
    }
}
