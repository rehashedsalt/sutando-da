﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Utilities;

namespace SutandoDa.Items
{
    public class ItemStarPlatinum : ItemStandSummoner
    {
        private readonly string[] jotaroNames =
        {
                "Jotaro".ToLower(),
                "Kujo Jotaro".ToLower(),
                "Jotaro Kujo".ToLower(),
                "JoJo".ToLower()
        };

        public override void SetStandSummonerStaticDefaults()
        {
            DisplayName.SetDefault("Hand Sigil");
            Tooltip.SetDefault("'Gimme a break'");
            standSummonMinDamage = 1;
            standAbilityMinDamage = 30;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.WorkBenches);
            recipe.AddIngredient(mod.GetItem("ItemStandIngredient"), 1);
            recipe.AddIngredient(ItemID.FallenStar, 5);
            recipe.AddIngredient(ItemID.PlatinumBar, 10);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }

        public override void ModifyStandDamage(Player player, ref float add, ref float mult, ref float flat)
        {
            // Figure out our damage
            // Bear in mind that punch ghost stands hit *stupid* fast
            flat = 4;

            // Pre-Hardmode

            if (player.statLifeMax >= 400) flat += 1;
            if (player.statManaMax >= 200) flat += 1;
            if (NPC.downedSlimeKing) flat += 2;
            if (NPC.downedBoss1) flat += 1; // Eye of Cthulhu
            if (NPC.downedBoss2) flat += 2; // Eater of Worlds/Brain of Cthulhu
            if (NPC.downedGoblins) flat += 1;
            if (NPC.downedBoss3) flat += 1; // Skeletron
            if (NPC.downedQueenBee) flat += 1;
            // At this point, the maximum possible damage to face WoFfle with is 14
            // This is actually really dumb, outpacing the Megashark on a 0def target, but trying to use a Punch Ghost against him is just asking for trouble, especially in expert or above

            // Hardmode

            // This damage proves to be alright for the mech bosses due to their increased defense
            // Additionally, you gain The World's time stop ability for the last fight, which is really powerful even at low duration (~3 seconds)
            if (NPC.downedMechBoss1) flat += 5;
            if (NPC.downedMechBoss2) flat += 5;
            if (NPC.downedMechBoss3) flat += 5;
            // Going into Planty you'll have 29 damage and time stop
            if (NPC.downedPlantBoss) flat += 5;
            if (NPC.downedGolemBoss) flat += 11;
            if (NPC.downedAncientCultist) flat += 11;
            // At the beginning of the Lunar events, you'll have 56 damage
            if (NPC.downedTowerNebula) flat += 7;
            if (NPC.downedTowerStardust) flat += 7;
            if (NPC.downedTowerSolar) flat += 7;
            if (NPC.downedTowerVortex) flat += 7;
            // And to fight Moonthulhu, you'll have 84
            if (NPC.downedMoonlord) flat += 50;
            if (player.statLifeMax2 >= 600) flat += 30;
            // And we're looking at 164 for Calamity-level content

            // And now for the easter egg
            // Because this only ever triggers past the Moon Lord fight, it's pretty tame
            // Calamity still adds more powerful items than this in-between Moonthulhu and the Profaned Goddess
            // Thorium, on the other hand...
            if (jotaroNames.Contains(player.name.ToLower()) && flat >= 120)
            {
                mult += 3;
            }
        }

        public override void StandModifyTooltips(List<TooltipLine> tooltips)
        {
            int damage = 0;
            GetStandDamage(Main.player[Main.myPlayer], ref damage);
            foreach (TooltipLine line in tooltips)
            {
                if (line.Name == "StandAbilityBuff")
                {
                    line.text = "-Left-click with stand withdrawn to stop time";
                }
            }
            
            if (damage < 15)
            {
                TooltipLine lowDamageSpeedWarning = new TooltipLine(mod, "StandPunchGhostSpeedNerf", "-50% attack speed until base 15 damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageSpeedWarning);
            }
            if (Main.ActivePlayersCount > 1 && damage >= standAbilityMinDamage)
            {
                TooltipLine friendlyFireWarning = new TooltipLine(mod, "StandPlayerHostileNote", "-Ability is hostile to non-teammates")
                {
                    isModifier = true,
                    overrideColor = new Color(0.914f, 0.714f, 0.533f)
                };
                tooltips.Add(friendlyFireWarning);
                if (jotaroNames.Contains(Main.player[Main.myPlayer].name.ToLower()))
                {
                    TooltipLine jotaroStarPlatinumBuff = new TooltipLine(mod, "StandStarPlatinumJotaroBuff", "-Jotaro's a lot better at punching things than stopping time")
                    {
                        isModifier = true,
                        overrideColor = new Color(1f, 1f, 0f)
                    };
                    tooltips.Add(jotaroStarPlatinumBuff);
                }
            }
        }

        public override bool StandSummon(Player player)
        {
            int damage = 0;
            GetStandDamage(player, ref damage);
            player.AddBuff(mod.BuffType("BuffStandStarPlatinum"), 600);
            Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("StandStarPlatinum"), damage, player.minionKB, Main.myPlayer);
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandStarPlatinumDeploy"), player.position);
            return true;
        }

        public override bool StandWithdraw(Player player)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            sPlayer.standExists = false;
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandStarPlatinumDeploy"), player.position);
            return true;
        }

        public override bool StandAbility2(Player player)
        {
            int damage = 0;
            GetStandDamage(player, ref damage);
            if (SutandoDaWorld.timeStopped > 0 || player.buffType.Contains(mod.BuffType("BuffTimeStopCooldown")) || player.buffType.Contains(mod.BuffType("BuffTimeStopTeam")) || damage < standAbilityMinDamage)
            {
                return false;
            }
            //Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("EffectAbilityTheWorld"), 0, 0, Main.myPlayer);
            Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("EffectTimeStop"), 0, 0, Main.myPlayer);
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandStarPlatinumAbilityActivate"), player.position);
            int time = damage;
            if (!jotaroNames.Contains(player.name.ToLower())) time *= 3;
            if (time > 420) time = 420;
            player.AddBuff(mod.BuffType("BuffTimeStop"), time);
            if (SutandoDaWorld.timeStopped < time) SutandoDaWorld.timeStopped = time;
            StandSummon(player);
            return true;
        }
    }
}
