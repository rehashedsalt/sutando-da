﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items
{
    public class ItemKillerQueen : ItemStandSummonerAbility
    {
        private readonly string[] kiraNames =
        {
                "Kira".ToLower(),
                "Kira Yoshikage".ToLower(),
                "Yoshikage Kira".ToLower(),
                "Yoshikage".ToLower()
        };

        public override void SetStandSummonerStaticDefaults()
        {
            DisplayName.SetDefault("100 Yen Coin");
            Tooltip.SetDefault("'Guaranteed to blow your mind'");
            standSummonMinDamage = 1;
            standAbilityMinDamage = 30;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.WorkBenches);
            recipe.AddIngredient(mod.GetItem("ItemStandIngredient"), 1);
            recipe.AddIngredient(ItemID.Bomb, 5);
            recipe.AddIngredient(ItemID.IronBar, 10);
            recipe.anyIronBar = true;
            recipe.AddIngredient(ItemID.Leather, 1);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }

        public override void ModifyStandDamage(Player player, ref float add, ref float mult, ref float flat)
        {
            flat = 30;
        }

        public override void StandModifyTooltips(List<TooltipLine> tooltips)
        {
            foreach (TooltipLine line in tooltips)
            {
                if (line.Name == "StandPrimaryAbility")
                {
                    line.text = "-Left-click with stand summoned to use primary bomb";
                }
                else if (line.Name == "StandAbilityBuff")
                {
                    line.text = "-Left-click with stand withdrawn to summon Sheer Heart Attack";
                }
            }
            if (kiraNames.Contains(Main.player[Main.myPlayer].name.ToLower()))
            {
                TooltipLine kiraKillerQueenBuff = new TooltipLine(mod, "StandKillerQueenKiraBuff", "-You just want to live a quiet life")
                {
                    isModifier = true,
                    overrideColor = new Color(1f, 1f, 0f)
                };
                tooltips.Add(kiraKillerQueenBuff);
            }
            if (Main.ActivePlayersCount > 1)
            {
                TooltipLine friendlyFireWarning = new TooltipLine(mod, "StandPlayerHostileNote", "-Abilities are hostile to non-teammates in PvP")
                {
                    isModifier = true,
                    overrideColor = new Color(0.914f, 0.714f, 0.533f)
                };
                tooltips.Add(friendlyFireWarning);
            }
        }

        public override bool StandSummon(Player player)
        {
            int damage = 0;
            GetStandDamage(player, ref damage);
            player.AddBuff(mod.BuffType("BuffStandKillerQueen"), 600);
            Vector2 summonPos = new Vector2(player.position.X, player.position.Y + player.height / 2 - 1);
            Projectile.NewProjectile(summonPos, new Vector2(0, 0), mod.ProjectileType("StandKillerQueen"), damage, player.minionKB, Main.myPlayer);
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenDeploy"), player.position);
            return true;
        }

        public override bool StandWithdraw(Player player)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            sPlayer.standExists = false;
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenWithdraw"), player.position);
            return true;
        }

        public override bool StandAbility1(Player player)
        {
            // Primary Bomb
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (sPlayer.killerQueenBomb == null)
            {
                Vector2 targetPos = Main.MouseWorld;
                // This snaps the position to the upper-left of the focused tile
                const int tileSize = 16;
                targetPos.X = targetPos.X - (targetPos.X % tileSize);
                targetPos.Y = targetPos.Y - (targetPos.Y % tileSize);
                // This then centers it on the tile
                targetPos.X += tileSize / 2;
                targetPos.Y += tileSize / 2;
                if (Vector2.Distance(player.position, targetPos) <= 128f && !player.buffType.Contains(mod.BuffType("DebuffBombCooldown")))
                {
                    sPlayer.killerQueenBomb = new KillerQueenBomb(mod, player, targetPos);
                    return true;
                }
                else return false;
            }
            else
            {
                Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), player.position);
                sPlayer.killerQueenBomb.Detonate();
                return true;
            }
        }

        public override bool StandAbility2(Player player)
        {
            // Sheer Heart Attack
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            const int maxWithdrawDistance = 768;
            const int frustrationBufferDistance = 512;
            if (sPlayer.sheerHeartAttackDeploy)
            {
                // Withdraw
                foreach (Projectile p in Main.projectile)
                {
                    if (p.owner == player.whoAmI && p.type == mod.ProjectileType("StandSheerHeartAttack"))
                    {
                        float distanceToSheer = Vector2.Distance(p.position, player.position);
                        Vector2 directionNormal = Vector2.Normalize(Vector2.Subtract(p.position, player.position));
                        if (distanceToSheer >= maxWithdrawDistance)
                        {
                            if (Main.myPlayer == player.whoAmI)
                            {
                                Vector2 targetPos = directionNormal * (maxWithdrawDistance - frustrationBufferDistance) + player.position;
                                Dust.NewDustPerfect(targetPos, mod.DustType("DustMenacing"));
                            }
                            return true;
                        }
                        else
                        {
                            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenWithdraw"), player.position);
                            sPlayer.sheerHeartAttackDeploy = false;
                            return true;
                        }
                    }
                }
                return true;
            }
            else if (!player.buffType.Contains(mod.BuffType("DebuffSHACooldown")))
            {
                // Summon
                int damage = 0;
                GetStandDamage(player, ref damage);
                sPlayer.sheerHeartAttackDeploy = true;
                player.AddBuff(mod.BuffType("BuffStandSheerHeartAttack"), 600);
                Vector2 summonPos = new Vector2(player.position.X, player.position.Y + player.height / 2 - 1);
                int sha = Projectile.NewProjectile(summonPos, new Vector2(0, 0), mod.ProjectileType("StandSheerHeartAttack"), damage, player.minionKB, Main.myPlayer);
                Main.projectile[sha].direction = player.direction;
                Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), player.position);
                return true;
            }
            else return false;
        }
    }
}
