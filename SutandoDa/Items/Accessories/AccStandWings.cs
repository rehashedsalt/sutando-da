﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items.Accessories
{
    [AutoloadEquip(EquipType.Wings)]
    public class AccStandWings : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Stand Power Aura");
            Tooltip.SetDefault("Allows flight and slow fall\nProduces particles");
        }

        public override void SetDefaults()
        {
            item.width = 32;
            item.height = 32;
            item.value = Item.sellPrice(0, 10, 0, 0);
            item.rare = 8;
            item.accessory = true;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            player.wingsLogic = 1;
            player.wingTimeMax = 180;
            player.flapSound = false;
        }

        public override bool WingUpdate(Player player, bool inUse)
        {
            if (inUse || player.GetModPlayer<SutandoDaPlayer>().standExists)
            {
                for (int i = 0; i < 20; i++)
                {
                    Vector2 velocity = new Vector2(Main.rand.NextFloat(-0.2f, 0.2f), Main.rand.NextFloat(-6f, -14f) + player.velocity.Y / 2);
                    Vector2 position = player.position;
                    position.Y += player.height + 8;
                    position.X -= 2.5f;

                    position.Y -= Main.rand.NextFloat(-6, player.height);
                    position.X += Main.rand.NextFloat(player.width / 2 * -1, player.width / 2);

                    //Dust.NewDust(position, 32, 32, mod.DustType("DustStandPowerGlow"), velocity.X, velocity.Y);
                    Dust.NewDustPerfect(position, mod.DustType("DustStandPowerGlow"), velocity);
                }
            }
            return true;
        }

        public override void VerticalWingSpeeds(Player player, ref float ascentWhenFalling, ref float ascentWhenRising, ref float maxCanAscendMultiplier, ref float maxAscentMultiplier, ref float constantAscend)
        {
            ascentWhenFalling = 0.85f;
            ascentWhenRising = 0.15f;
            maxCanAscendMultiplier = 1f;
            maxAscentMultiplier = 3f;
            constantAscend = 0.135f;
        }

        public override void HorizontalWingSpeeds(Player player, ref float speed, ref float acceleration)
        {
            speed = 9f;
            acceleration *= 2.5f;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.LunarCraftingStation);
            recipe.AddIngredient(ItemID.FragmentSolar, 4);
            recipe.AddIngredient(ItemID.FragmentVortex, 4);
            recipe.AddIngredient(ItemID.FragmentNebula, 4);
            recipe.AddIngredient(ItemID.FragmentStardust, 4);
            recipe.AddIngredient(ItemID.LunarBar, 10);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
