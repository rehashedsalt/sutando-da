﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items.Armor
{
    [AutoloadEquip(EquipType.Head)]
    public class CapJotaroWhite : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("White Cap");
            Tooltip.SetDefault("'Crazy noisy bizzare town edition'");
        }

        public override void SetDefaults()
        {
            item.value = Item.sellPrice(0, 0, 50, 0);
            item.width = 22;
            item.height = 10;
            item.vanity = true;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.Loom);
            recipe.AddIngredient(mod.GetItem("CapJotaroBlack"), 1);
            recipe.AddIngredient(ItemID.HallowedBar, 3);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
