﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items.Armor
{
    [AutoloadEquip(EquipType.Head)]
    public class CapJotaroBlack : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Black Cap");
            Tooltip.SetDefault("'Yare yare daze'");
        }

        public override void SetDefaults()
        {
            item.value = Item.sellPrice(0, 0, 50, 0);
            item.width = 22;
            item.height = 10;
            item.vanity = true;
        }

        public override void AddRecipes()
        {
            // Gold
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.Loom);
            recipe.AddIngredient(ItemID.Silk, 5);
            recipe.AddIngredient(ItemID.GoldBar, 1);
            recipe.SetResult(this);
            recipe.AddRecipe();

            // Platinum
            recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.Loom);
            recipe.AddIngredient(ItemID.Silk, 5);
            recipe.AddIngredient(ItemID.PlatinumBar, 1);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }
    }
}
