﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items
{
    public abstract class ItemStandSummoner : ModItem
    {
        public static int standSummonMinDamage = 1;
        public static int standAbilityMinDamage = 0;

        public override void SetStaticDefaults()
        {
            SetStandSummonerStaticDefaults();
        }

        public override void SetDefaults()
        {
            item.damage = 10;
            item.knockBack = 3;
            item.noMelee = true;
            item.useTime = 20;
            item.useTurn = true;
            item.useAnimation = 20;
            item.useStyle = 4;
            item.autoReuse = false;

            item.value = Item.sellPrice(1, 0, 0, 0);
            item.rare = 8; // Yellow

            SetStandSummonerDefaults();
        }

        public override void ModifyWeaponDamage(Player player, ref float add, ref float mult, ref float flat)
        {
            ModifyStandDamage(player, ref add, ref mult, ref flat);
            flat -= item.damage;
        }

        public abstract void ModifyStandDamage(Player player, ref float add, ref float mult, ref float flat);

        public virtual void GetStandDamage(Player player, ref int damage)
        {
            float add = 0;
            float mult = 1;
            float fDamage = damage;
            ModifyStandDamage(player, ref add, ref mult, ref fDamage);
            damage = (int)fDamage;
        }

        public override void ModifyTooltips(List<TooltipLine> tooltips)
        {
            int damage = 0;
            GetStandDamage(Main.player[Main.myPlayer], ref damage);
            TooltipLine standInfoHeader = new TooltipLine(mod, "StandInfoHeader", "Stands grow as you accomplish feats");
            tooltips.Add(standInfoHeader);
            string[] removeTooltips =
            {
                "CritChance",
                "Speed",
                "Knockback"
            };
            foreach (TooltipLine line in tooltips.ToList())
            {
                if (removeTooltips.Contains(line.Name))
                {
                    tooltips.Remove(line);
                }
            }
            if (damage < standSummonMinDamage)
            {
                TooltipLine lowDamageSummonWarning = new TooltipLine(mod, "StandSummonNerf", "-Cannot summon stand until " + standSummonMinDamage + " damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageSummonWarning);
            }
            else
            {
                TooltipLine highDamageSummonNotification = new TooltipLine(mod, "StandSummonBuff", "-Right-click to toggle stand")
                {
                    isModifier = true
                };
                TooltipLine highDamageSummonNotification2 = new TooltipLine(mod, "StandTarget", "-Left-click with stand summoned to designate a target")
                {
                    isModifier = true
                };
                tooltips.Add(highDamageSummonNotification);
                tooltips.Add(highDamageSummonNotification2);
            }
            if (damage < standAbilityMinDamage)
            {
                TooltipLine lowDamageAbilityWarning = new TooltipLine(mod, "StandAbilityNerf", "-Cannot use ability until " + standAbilityMinDamage + " damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageAbilityWarning);
            }
            else
            {
                TooltipLine highDamageAbilityNotification = new TooltipLine(mod, "StandAbilityBuff", "-Left-click with stand withdrawn to use ability")
                {
                    isModifier = true
                };
                tooltips.Add(highDamageAbilityNotification);
            }
            StandModifyTooltips(tooltips);
        }

        public override bool UseItem(Player player)
        {
            int damage = 0;
            GetStandDamage(Main.player[Main.myPlayer], ref damage);
            // Return if we're alt-firing
            if (player.altFunctionUse == 2) return true;
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (sPlayer.standExists)
            {
                return StandAbility1(player);
            }
            else if (damage >= standAbilityMinDamage)
            {
                return StandAbility2(player);
            }
            else
            {
                return false;
            }
        }

        public override bool AltFunctionUse(Player player)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (sPlayer.standExists)
            {
                return StandWithdraw(player);
            }
            else if (item.damage >= standSummonMinDamage)
            {
                return StandSummon(player);
            }
            else
            {
                return false;
            }
        }

        public virtual void SetStandSummonerStaticDefaults() { return; }
        public virtual void SetStandSummonerDefaults() { return; }
        public virtual void StandModifyTooltips(List<TooltipLine> tooltips) { return; }

        public abstract bool StandSummon(Player player);
        public abstract bool StandWithdraw(Player player);
        public virtual bool StandAbility1(Player player)
        {
            player.MinionNPCTargetAim();
            Main.PlaySound(SoundID.Item8, player.position);
            return true;
        }
        public abstract bool StandAbility2(Player player);
    }

    public abstract class ItemStandSummonerAbility : ItemStandSummoner
    {

        public override void ModifyTooltips(List<TooltipLine> tooltips)
        {
            int damage = 0;
            GetStandDamage(Main.player[Main.myPlayer], ref damage);
            TooltipLine standInfoHeader = new TooltipLine(mod, "StandInfoHeader", "Stands grow as you accomplish feats");
            tooltips.Add(standInfoHeader);
            string[] removeTooltips =
            {
                "Damage",
                "CritChance",
                "Speed",
                "Knockback"
            };
            foreach (TooltipLine line in tooltips.ToList())
            {
                if (removeTooltips.Contains(line.Name))
                {
                    tooltips.Remove(line);
                }
            }
            if (damage < standSummonMinDamage)
            {
                TooltipLine lowDamageSummonWarning = new TooltipLine(mod, "StandSummonNerf", "-Cannot summon stand until " + standSummonMinDamage + " damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageSummonWarning);
            }
            else
            {
                TooltipLine highDamageSummonNotification = new TooltipLine(mod, "StandSummonBuff", "-Right-click to toggle stand")
                {
                    isModifier = true
                };
                TooltipLine highDamageSummonNotification2 = new TooltipLine(mod, "StandPrimaryAbility", "-Left-click with stand summoned to use primary ability")
                {
                    isModifier = true
                };
                tooltips.Add(highDamageSummonNotification);
                tooltips.Add(highDamageSummonNotification2);
            }
            if (damage < standAbilityMinDamage)
            {
                TooltipLine lowDamageAbilityWarning = new TooltipLine(mod, "StandAbilityNerf", "-Cannot use secondary ability until " + standAbilityMinDamage + " damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageAbilityWarning);
            }
            else
            {
                TooltipLine highDamageAbilityNotification = new TooltipLine(mod, "StandAbilityBuff", "-Left-click with stand withdrawn to use secondary ability")
                {
                    isModifier = true
                };
                tooltips.Add(highDamageAbilityNotification);
            }
            StandModifyTooltips(tooltips);
        }
    }
}
