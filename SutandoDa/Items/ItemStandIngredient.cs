﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Items
{
    public class ItemStandIngredient : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Soul of Fight");
            Tooltip.SetDefault("'The essence of your own fighting spirit'");
            Main.RegisterItemAnimation(item.type, new DrawAnimationVertical(5, 4));
            ItemID.Sets.AnimatesAsSoul[item.type] = true;
            ItemID.Sets.ItemIconPulse[item.type] = true;
            ItemID.Sets.ItemNoGravity[item.type] = true;
        }

        public override void SetDefaults()
        {
            item.width = 22;
            item.height = 22;
            item.maxStack = 999;
            item.rare = 11;
        }

        public override bool PreDrawInWorld(SpriteBatch spriteBatch, Color lightColor, Color alphaColor, ref float rotation, ref float scale, int whoAmI)
        {
            Lighting.AddLight(item.position, 0.239f, 0.122f, 0.349f);
            if (SutandoDaWorld.timeStopped > 0) return false;
            if (Main.time % 60 == 0)
            {
                Vector2 dustPos = new Vector2(item.position.X + Main.rand.NextFloat(-24f, 24f), item.position.Y + Main.rand.NextFloat(-24f, 24f));
                Dust.NewDust(dustPos, 32, 32, mod.DustType("DustMenacing"), 0f, 1f);
            }
            return true;
        }
    }
}
