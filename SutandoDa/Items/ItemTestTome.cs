﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.DataStructures;

namespace SutandoDa.Items
{
    public class ItemTestTome : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Salt's Tome of Testing");
            Tooltip.SetDefault("Grants omniscience");
        }

        public override void SetDefaults()
        {
            item.useTime = 20;
            item.useAnimation = 20;
            item.useStyle = 5;
            item.UseSound = SoundID.DD2_ExplosiveTrapExplode; // Demon Scythe
            item.autoReuse = true;
            
            item.value = 10000;
            item.rare = -12; // Rainbow
        }

        public override bool UseItem(Player player)
        {
            //player.Hurt(PlayerDeathReason.ByCustomReason(player.name + " bit the dust"), 150, 0);
            //Projectile.NewProjectile(player.position, new Vector2(0,0), mod.ProjectileType("ExplosionKillerQueen"), 100, 0, Main.myPlayer);
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            sPlayer.killerQueenBomb = new KillerQueenBomb(mod, player, player);
            Dust.NewDust(player.position, 32, 32, mod.DustType("DustHitHardDark"));
            return true;
        }

        public override bool AltFunctionUse(Player player)
        {
            Main.NewText("----------------------", 255, 255, 0);
            Main.NewText("Stand Status", 255, 255, 128);
            if (!(player.statLifeMax >= 400)) Main.NewText("Needs 400 health", 128, 128, 128);
            if (!(player.statLifeMax2 >= 600)) Main.NewText("Needs 600 health (can be with equipment/buffs)", 128, 128, 128);
            if (!(player.statManaMax >= 200)) Main.NewText("Needs 200 mana", 128, 128, 128);
            if (!NPC.downedSlimeKing) Main.NewText("Hasn't defeated Slime King", 128, 128, 128);
            if (!NPC.downedBoss1) Main.NewText("Hasn't defeated Boss 1 (Eye of Cthulhu)", 128, 128, 128);
            if (!NPC.downedBoss2) Main.NewText("Hasn't defeated Boss 2 (Eater/Brain)", 128, 128, 128);
            if (!NPC.downedGoblins) Main.NewText("Hasn't defeated a Goblin Invasion", 128, 128, 128);
            if (!NPC.downedBoss3) Main.NewText("Hasn't defeated Boss 3 (Skeletron)", 128, 128, 128);
            if (!NPC.downedQueenBee) Main.NewText("Hasn't defeated Queen Bee", 128, 128, 128);
            if (!NPC.downedMechBoss1) Main.NewText("Hasn't defeated Mech Boss 1 (Twins?)", 128, 128, 128);
            if (!NPC.downedMechBoss2) Main.NewText("Hasn't defeated Mech Boss 2 (Destroyer?)", 128, 128, 128);
            if (!NPC.downedMechBoss3) Main.NewText("Hasn't defeated Mech Boss 3 (Skeletron Prime?)", 128, 128, 128);
            if (!NPC.downedPlantBoss) Main.NewText("Hasn't defeated Plantera", 128, 128, 128);
            if (!NPC.downedGolemBoss) Main.NewText("Hasn't defeated Golem", 128, 128, 128);
            if (!NPC.downedAncientCultist) Main.NewText("Hasn't defeated the Ancient Cultist", 128, 128, 128);
            if (!NPC.downedTowerNebula) Main.NewText("Hasn't defeated the Nebula", 128, 128, 128);
            if (!NPC.downedTowerStardust) Main.NewText("Hasn't defeated the Stardust", 128, 128, 128);
            if (!NPC.downedTowerSolar) Main.NewText("Hasn't defeated the Solar", 128, 128, 128);
            if (!NPC.downedTowerVortex) Main.NewText("Hasn't defeated the Vortex", 128, 128, 128);
            if (!NPC.downedMoonlord) Main.NewText("Hasn't defeated Moonthulhu", 128, 128, 128);
            Main.NewText("World Variables", 255, 128, 128);
            Main.NewText("SutandoDaWorld.timeStopped: " + SutandoDaWorld.timeStopped, 128, 128, 128);
            Main.NewText("Main.time: " + Main.time, 128, 128, 128);
            Main.NewText("Player Variables", 128, 255, 128);
            Main.NewText("Position: x" + player.position.X + " y" + player.position.Y + " facing " + player.direction, 128, 128, 128);
            Main.NewText("Velocity: x" + player.velocity.X + " y" + player.velocity.Y, 128, 128, 128);
            Main.NewText("sPlayer Variables", 128, 128, 255);
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            Main.NewText("sPlayer.standExists: " + sPlayer.standExists, 128, 128, 128);
            Main.NewText("sPlayer.sheerHeartAttackDeploy: " + sPlayer.sheerHeartAttackDeploy, 128, 128, 128);
            Main.NewText("PlayerStop Variables", 128, 128, 255);
            PlayerStop stopPlayer = player.GetModPlayer<PlayerStop>();
            Main.NewText("timeStopDamage: " + stopPlayer.timeStopBuildupDamage + " direction " + stopPlayer.timeStopBuildupHitDirection, 128, 128, 128);
            Main.NewText("timeStopVelocity: x" + stopPlayer.timeStopVelocity.X + " y" + stopPlayer.timeStopVelocity.Y + " " + stopPlayer.timeStopVelocityApplied, 128, 128, 128);
            return true;
        }
    }
}
