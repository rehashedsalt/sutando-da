﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Utilities;

namespace SutandoDa.Items
{
    public class ItemTheWorld : ItemStandSummoner
    {
        public override void SetStandSummonerStaticDefaults()
        {
            DisplayName.SetDefault("Green Heart");
            Tooltip.SetDefault("'Useless'");
            standSummonMinDamage = 1;
            standAbilityMinDamage = 20;
        }

        public override void AddRecipes()
        {
            ModRecipe recipe = new ModRecipe(mod);
            recipe.AddTile(TileID.WorkBenches);
            recipe.AddIngredient(mod.GetItem("ItemStandIngredient"), 1);
            recipe.AddIngredient(ItemID.FallenStar, 5);
            recipe.AddIngredient(ItemID.GoldBar, 10);
            recipe.SetResult(this);
            recipe.AddRecipe();
        }

        public override void ModifyStandDamage(Player player, ref float add, ref float mult, ref float flat)
        {
            // Figure out our damage
            // Bear in mind that punch ghost stands hit *stupid* fast
            flat = 4;

            // Pre-Hardmode

            if (player.statLifeMax >= 400) flat += 1;
            if (player.statManaMax >= 200) flat += 1;
            if (NPC.downedSlimeKing) flat += 2;
            if (NPC.downedBoss1) flat += 1; // Eye of Cthulhu
            if (NPC.downedBoss2) flat += 2; // Eater of Worlds/Brain of Cthulhu
            if (NPC.downedGoblins) flat += 1;
            if (NPC.downedBoss3) flat += 1; // Skeletron
            if (NPC.downedQueenBee) flat += 1;
            // At this point, the maximum possible damage to face WoFfle with is 14
            // This is actually really dumb, outpacing the Megashark on a 0def target, but trying to use a Punch Ghost against him is just asking for trouble, especially in expert or above

            // Hardmode

            // This damage proves to be alright for the mech bosses due to their increased defense
            // Additionally, you gain The World's time stop ability for the last fight, which is really powerful even at low duration (~3 seconds)
            if (NPC.downedMechBoss1) flat += 4;
            if (NPC.downedMechBoss2) flat += 4;
            if (NPC.downedMechBoss3) flat += 4;
            // Going into Planty you'll have 26 damage and time stop
            if (NPC.downedPlantBoss) flat += 4;
            if (NPC.downedGolemBoss) flat += 10;
            if (NPC.downedAncientCultist) flat += 10;
            // At the beginning of the Lunar events, you'll have 50 damage
            if (NPC.downedTowerNebula) flat += 5;
            if (NPC.downedTowerStardust) flat += 5;
            if (NPC.downedTowerSolar) flat += 5;
            if (NPC.downedTowerVortex) flat += 5;
            // And to fight Moonthulhu, you'll have 70
            if (NPC.downedMoonlord) flat += 50;
            if (player.statLifeMax2 >= 600) flat += 30;
            // And we're looking at 150 for Calamity-level content
        }

        public override void StandModifyTooltips(List<TooltipLine> tooltips)
        {
            int damage = 0;
            GetStandDamage(Main.player[Main.myPlayer], ref damage);
            foreach (TooltipLine line in tooltips)
            {
                if (line.Name == "StandAbilityBuff")
                {
                    line.text = "-Left-click with stand withdrawn to stop time";
                }
            }
            if (damage < 15)
            {
                TooltipLine lowDamageSpeedWarning = new TooltipLine(mod, "StandPunchGhostSpeedNerf", "-50% attack speed until base 15 damage")
                {
                    isModifier = true,
                    isModifierBad = true
                };
                tooltips.Add(lowDamageSpeedWarning);
            }
            if (Main.player[Main.myPlayer].name.ToLower() == "Dio".ToLower() && damage >= standAbilityMinDamage)
            {
                TooltipLine dioTheWorldBuff = new TooltipLine(mod, "StandWorldDioBuff", "-Lord DIO can use The World's abilities better than any other")
                {
                    isModifier = true,
                    overrideColor = new Color(1f, 1f, 0f)
                };
                tooltips.Add(dioTheWorldBuff);
            }
            if (Main.ActivePlayersCount > 1 && damage >= standAbilityMinDamage)
            {
                TooltipLine friendlyFireWarning = new TooltipLine(mod, "StandPlayerHostileNote", "-Ability is hostile to non-teammates")
                {
                    isModifier = true,
                    overrideColor = new Color(0.914f, 0.714f, 0.533f)
                };
                tooltips.Add(friendlyFireWarning);
            }
        }

        public override bool StandSummon(Player player)
        {
            int damage = 0;
            GetStandDamage(player, ref damage);
            player.AddBuff(mod.BuffType("BuffStandTheWorld"), 600);
            Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("StandTheWorld"), damage, player.minionKB, Main.myPlayer);
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandWorldDeploy"), player.position);
            return true;
        }

        public override bool StandWithdraw(Player player)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            sPlayer.standExists = false;
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandWorldDeploy"), player.position);
            return true;
        }

        public override bool StandAbility2(Player player)
        {
            int damage = 0;
            GetStandDamage(player, ref damage);
            if (SutandoDaWorld.timeStopped > 0 || player.buffType.Contains(mod.BuffType("BuffTimeStopCooldown")) || player.buffType.Contains(mod.BuffType("BuffTimeStopTeam")) || damage < standAbilityMinDamage)
            {
                return false;
            }
            Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("EffectAbilityTheWorld"), 0, 0, Main.myPlayer);
            Projectile.NewProjectile(player.position, new Vector2(0, 0), mod.ProjectileType("EffectTimeStop"), 0, 0, Main.myPlayer);
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandWorldAbilityActivate"), player.position);
            int time = damage * 10;
            if (time > 420) time = 420;
            // ゴゴゴゴゴゴゴゴゴゴゴゴゴゴゴ
            if (player.name.ToLower() == "Dio".ToLower()) time += 180;
            player.AddBuff(mod.BuffType("BuffTimeStop"), time);
            if (SutandoDaWorld.timeStopped < time) SutandoDaWorld.timeStopped = time;
            StandSummon(player);
            return true;
        }
    }
}
