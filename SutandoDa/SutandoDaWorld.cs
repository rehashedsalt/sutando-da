﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.ModLoader.IO;

namespace SutandoDa
{
    class SutandoDaWorld : ModWorld
    {
        private static int lastTimeStopped = 0;
        public static int timeStopped = 0;

        public override void Load(TagCompound tag)
        {
            timeStopped = 0;
            base.Load(tag);
        }

        public override void NetSend(BinaryWriter writer)
        {
            byte timeStopByte = 0;
            if (timeStopped >= 255)
            {
                timeStopByte = 255;
            }
            else
            {
                timeStopByte = (byte)timeStopped;
            }
            writer.Write(timeStopByte);
        }

        public override void NetReceive(BinaryReader reader)
        {
            timeStopped = reader.ReadByte();
        }

        public override void PreUpdate()
        {
            if (timeStopped > 0)
            {
                timeStopped--;
                if (timeStopped == 0 || timeStopped == 92 || timeStopped == 91 || lastTimeStopped == 0)
                {
                    NetMessage.SendData(MessageID.WorldData);
                }
            }
            lastTimeStopped = timeStopped;
        }
    }
}
