﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa
{
    public class SutandoDaPlayer : ModPlayer
    {
        public bool standExists = false;
        public KillerQueenBomb killerQueenBomb = null;
        public bool sheerHeartAttackDeploy = false;

        private readonly string[] standBuffs =
        {
            "BuffStandTheWorld",
            "BuffStandStarPlatinum",
            "BuffStandKillerQueen"
        };

        public override void PreUpdate()
        {
            // Killer Queen bomb display status
            if (killerQueenBomb != null) killerQueenBomb.Validate();
            if (killerQueenBomb != null)
            {
                if (Main.player[Main.myPlayer] == player)
                {
                    Dust.NewDustPerfect(killerQueenBomb.Position(), mod.DustType("DustKillerQueenEvil"));
                    Lighting.AddLight(killerQueenBomb.Position(), 0.933f, 0.875f, 0.910f);
                }
                if (!standExists)
                {
                    float granularity = 0.5f + ((float)Main.time % 10f / 10f);
                    // Only attempt to auto-detonate if Killer Queen isn't out
                    // Also denote the auto detonation range
                    Vector2 dustOffsetPos = new Vector2(0, killerQueenBomb.triggerDistance);
                    for (float i = 0; i < (Math.PI * 2); i += granularity)
                    {
                        Dust.NewDustPerfect(killerQueenBomb.Position() + dustOffsetPos, mod.DustType("DustKillerQueenBright"));
                        dustOffsetPos = Vector2.Transform(dustOffsetPos, Matrix.CreateRotationZ(granularity));
                    }
                    killerQueenBomb.TryAutoDetonate();
                }
            }
            // If the player doesn't have a stand, update the variable
            foreach (string stand in standBuffs)
            {
                if (player.buffType.Contains(mod.BuffType(stand)))
                {
                    return;
                }
            }
            if (standExists)
            {
                standExists = false;
                return;
            }
            // If player has SHA, ensure they get the buff
            if (sheerHeartAttackDeploy && !player.buffType.Contains(mod.BuffType("BuffSheerHeartAttack")))
            {
                player.AddBuff(mod.BuffType("BuffSheerHeartAttack"), 60);
            }
        }

        [Obsolete]
        public override void SetupStartInventory(IList<Item> items)
        {
            items.Add(mod.GetItem("ItemStandIngredient").item);
        }

        public override void UpdateBiomeVisuals()
        {
            player.ManageSpecialBiomeVisuals("SutandoDa:TimeStop", SutandoDaWorld.timeStopped > 0);
        }

        public override void ResetEffects()
        {
            standExists = false;
        }
    }
}
