﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    /// <summary>
    /// The buff given while SHA is deployed
    /// This is different from the other stand buffs, which set the stand flag on the player
    /// Instead, this listens for the flag and dies when it becomes unset
    /// Additionally, a check in the player update function puts this on a player who has SHA but not the buff
    /// </summary>
    public class BuffStandSheerHeartAttack : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Sheer Heart Attack");
            Description.SetDefault("It has no weaknesses");
            canBeCleared = false;

            Main.buffNoTimeDisplay[Type] = true;
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>(mod);
            player.buffTime[buffIndex] = 600;
            if (!(player.ownedProjectileCounts[mod.ProjectileType("StandSheerHeartAttack")] > 0)) sPlayer.sheerHeartAttackDeploy = false;
            if (!sPlayer.sheerHeartAttackDeploy) player.buffTime[buffIndex] = 0;
        }
    }
}
