﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    public class BuffTimeStop : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Toki Yo Tomare");
            Description.SetDefault("Time has stopped, but for how long?");
            canBeCleared = false;

            Main.buffNoTimeDisplay[Type] = true;
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            PlayerStop stopPlayer = player.GetModPlayer<PlayerStop>();
            // Give all non-time-stopped players the corresponding buffs and debuffs
            if (player != Main.player[Main.myPlayer] && !Main.player[Main.myPlayer].buffType.Contains(mod.BuffType("BuffTimeStop")))
            {
                if (player.team == Main.player[Main.myPlayer].team)
                {
                    Main.player[Main.myPlayer].AddBuff(mod.BuffType("BuffTimeStopTeam"), 20);
                }
                else
                {
                    Main.player[Main.myPlayer].AddBuff(mod.BuffType("DebuffTimeStop"), 20);
                }
            }
            // Play the sound a little early to try and time its climax with time actually resuming
            if (SutandoDaWorld.timeStopped == 92)
            {
                Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/StandWorldAbilityDispel"), player.position);
            }
            if (player.buffTime[buffIndex] == 0)
            {
                int cooldown = 1800;
                foreach (NPC npc in Main.npc)
                {
                    if (npc.boss) cooldown = 3600;
                }
                stopPlayer.timeStopImmune = false;
                player.AddBuff(mod.BuffType("BuffTimeStopCooldown"), cooldown + 60);
                player.DelBuff(buffIndex);
                buffIndex--;
            }
            stopPlayer.timeStopImmune = true;
        }
    }
}
