﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

// All credit for this buff and related code goes to the Antiaris mod
// Seriously, go check that mod out. Shit's awesome.

namespace SutandoDa.Buffs
{
    public class BuffTimeStopCooldown : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Chrono Fatigue");
            Description.SetDefault("You cannot stop time");
            canBeCleared = false;
            
            Main.debuff[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            // Time stopping also stops our recovery
            if (player.buffType.Contains(mod.BuffType("DebuffTimeStop")))
            {
                player.buffTime[buffIndex]++;
            }
        }
    }
}
