﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    public class DebuffBombCooldown : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Primary Bomb Recharge");
            Description.SetDefault("You cannot lay Killer Queen's primary bomb");
            canBeCleared = false;
            
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }
    }
}
