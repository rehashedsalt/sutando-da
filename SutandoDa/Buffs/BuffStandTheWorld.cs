﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    /// <summary>
    /// The buff given while you have The World stand
    /// </summary>
    class BuffStandTheWorld : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Stand Power");
            Description.SetDefault("You are being possessed by an evil spirit");

            Main.buffNoTimeDisplay[Type] = true;
            Main.buffNoSave[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>(mod);
            player.buffTime[buffIndex] = 600;
            if (!sPlayer.standExists) sPlayer.standExists = true;
            if (!(player.ownedProjectileCounts[mod.ProjectileType("StandTheWorld")] > 0)) player.buffTime[buffIndex] = 0;
        }
    }
}
