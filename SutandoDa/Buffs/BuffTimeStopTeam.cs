﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    public class BuffTimeStopTeam : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Toki Yo Tomare");
            Description.SetDefault("A member of your team has halted the flow of time");
            canBeCleared = false;

            Main.buffNoTimeDisplay[Type] = true;
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            PlayerStop stopPlayer = player.GetModPlayer<PlayerStop>();
            stopPlayer.timeStopImmune = true;
            if (SutandoDaWorld.timeStopped > 0)
            {
                player.buffTime[buffIndex] = 60;
            }
            else
            {
                int cooldown = 900;
                foreach (NPC npc in Main.npc)
                {
                    if (npc.boss) cooldown = 2700;
                }
                player.buffTime[buffIndex] = 0;
                player.AddBuff(mod.BuffType("BuffTimeStopCooldown"), cooldown + 60);
            }
        }
    }
}
