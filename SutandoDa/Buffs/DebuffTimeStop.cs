﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    public class DebuffTimeStop : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Frozen in Time");
            Description.SetDefault("You can't move a muscle!\nThis must be the work of an enemy stand!");
            canBeCleared = false;

            Main.buffNoTimeDisplay[Type] = true;
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }

        public override void Update(Player player, ref int buffIndex)
        {
            PlayerStop stopPlayer = player.GetModPlayer<PlayerStop>();
            stopPlayer.timeStopImmune = false;
            if (!(SutandoDaWorld.timeStopped > 0) || player.buffType.Contains(mod.BuffType("BuffTimeStop")))
            {
                player.buffTime[buffIndex] = 0;
            }
            else
            {
                player.buffTime[buffIndex] = 60;
            }
        }
    }
}
