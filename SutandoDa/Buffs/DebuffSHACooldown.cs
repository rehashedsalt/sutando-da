﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Buffs
{
    public class DebuffSHACooldown : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Sheer Heart Attack Recharge");
            Description.SetDefault("It's not a weakness, it just needs to recharge");
            canBeCleared = false;
            
            Main.buffNoSave[Type] = true;
            Main.debuff[Type] = true;
        }
    }
}
