using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ModLoader;

namespace SutandoDa
{
	class SutandoDa : Mod
	{
		public SutandoDa()
		{
            Properties = new ModProperties()
            {
                Autoload = true,
                AutoloadSounds = true
            };
        }

        public override void Load()
        {
            if (!Main.dedServ)
            {
                Filters.Scene["SutandoDa:TimeStop"] = new Filter(new ScreenShaderData("FilterMiniTower").UseColor(0.6f, 0.6f, 0.6f), EffectPriority.VeryHigh);
                SkyManager.Instance["SutandoDa:TimeStop"] = new TimeSky();
            }
        }
    }
}
