﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.Audio;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Minions
{
    /// <summary>
    /// A basic stand template
    /// </summary>
    public abstract class Stand : ModProjectile
    {
        public bool timeStopImmune = false;

        public override void AI()
        {
            CheckActive();
            if (!(SutandoDaWorld.timeStopped > 0) || !timeStopImmune)
            {
                Behavior();
            }
        }

        public abstract void CheckActive();
        public abstract void Behavior();
    }

    /// <summary>
    /// Star Platinum, The World, etc. type stands
    /// </summary>
    public abstract class StandTemplatePunchGhost : Stand
    {
        /// <summary>
        /// How many pixels the stand should hover away from the player
        /// A tile in Terraria is 16 pixels squared
        /// </summary>
        protected Vector2 aiSpacing = new Vector2(24f, 16f);
        /// <summary>
        /// The stand will hit targets once every this frames
        /// </summary>
        protected int aiHitSpeed = 3;
        /// <summary>
        /// How quickly the stand should accelerate toward the player when drifting close by
        /// </summary>
        protected float aiIdleAccel = 0.32f;
        /// <summary>
        /// The maximum distance that the stand is allowed to be when fighting a boss
        /// </summary>
        protected float aiMaxBossEngagementDistance = 320f;
        /// <summary>
        /// How quickly the stand should accelerate toward an engaged target
        /// </summary>
        protected float aiEngageAccel = 0.3f;
        /// <summary>
        /// The maximum distance that an NPC can be for the stand to engage it
        /// </summary>
        protected float aiMaxEngagementDistance = 256f;
        /// <summary>
        /// What projectile a punch ghost should launch as his fist
        /// </summary>
        public int fistProjectile;

        public override void CheckActive()
        {
            Player player = Main.player[projectile.owner];
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (player.dead)
            {
                sPlayer.standExists = false;
                projectile.timeLeft = 0;
            }
            if (sPlayer.standExists)
            {
                projectile.timeLeft = 2;
            }
        }

        public override void Behavior()
        {
            // Boilerplate
            projectile.direction = projectile.spriteDirection;
            // First priority is to find and engage targets
            if (AIEngage()) return;
            // After that, teleport back to the player if they run *way* far away
            if (AIChaseFar()) return;
            // Then finally, when there aren't any enemies around, stand next to the player
            AIChaseNear();
        }

        private bool AIChaseFar()
        {
            Player player = Main.player[projectile.owner];
            if (Vector2.Distance(player.position, projectile.position) >= aiMaxEngagementDistance)
            {
                projectile.position = player.position;
                return true;
            }
            else return false;
        }

        private bool AIEngage()
        {
            NPC target = null;
            Player player = Main.player[projectile.owner];
            Vector2 footPos = new Vector2(projectile.position.X + (projectile.width / 2), projectile.position.Y + projectile.height / 2);

            // Find someone to love us dearly
            if (player.HasMinionAttackTargetNPC)
            {
                NPC possibleTarget = Main.npc[player.MinionAttackTargetNPC];
                if (!possibleTarget.friendly && !possibleTarget.immortal && possibleTarget.life > 0 &&
                    ((possibleTarget.boss && Vector2.Distance(possibleTarget.position, player.position) <= aiMaxBossEngagementDistance) || (!possibleTarget.boss && Vector2.Distance(possibleTarget.position, player.position) <= aiMaxEngagementDistance))
                    )
                {
                    target = possibleTarget;
                }
                else
                {
                    target = null;
                }
            }
            if (target == null)
            {
                foreach (NPC possibleTarget in Main.npc)
                {
                    if (!possibleTarget.friendly && !possibleTarget.immortal && possibleTarget.life > 0 && possibleTarget.CanBeChasedBy((object)this, false) && Collision.CanHitLine(player.position, player.width, player.height, possibleTarget.position, possibleTarget.width, possibleTarget.height))
                    {
                        if (target == null || target.immune[0] != 0 || Vector2.Distance(possibleTarget.position, player.position) < Vector2.Distance(target.position, player.position))
                        {
                            target = possibleTarget;
                        }
                    }
                }
            }
            
            // Ensure it's not old and stale or dead or null or out of range or something
            if (target == null || target.life < 1 )
            {
                target = null;
                return false;
            }
            if (target.boss && Vector2.Distance(target.position, player.position) >= aiMaxBossEngagementDistance)
            {
                // Bosses have a further engagement distance
                target = null;
                return false;
            }
            else if (Vector2.Distance(target.position, player.position) >= aiMaxEngagementDistance)
            {
                target = null;
                return false;
            }

            // Animation
            // Make sure we're facing the right direction
            if (footPos.X > target.position.X)
            {
                projectile.spriteDirection = -1;
            }
            else
            {
                projectile.spriteDirection = 1;
            }

            // Try to get beside the target so we can punch them to death
            Vector2 targetPos = new Vector2(target.position.X - (48f * projectile.spriteDirection), target.position.Y);

            // Behavior
            // All paths here *need* to return some value
            if (Vector2.Distance(targetPos, footPos) <= 64f)
            {
                // We're within punching range and should beat the shit out of it
                projectile.velocity = Vector2.Multiply(projectile.velocity, 0.9f);

                // Play some special gunshot sounds if we're fighting a big bad or if we're dumb powerful
                if (Main.time % 6 == 0 && (target.boss || projectile.damage >= 100))
                {
                    LegacySoundStyle sound = null;
                    if (projectile.damage >= 15 && projectile.damage < 30)
                    {
                        // Pew pew
                        sound = new LegacySoundStyle(2, 11, Terraria.Audio.SoundType.Sound);
                    }
                    else if (projectile.damage >= 30)
                    {
                        // Boom boom
                        sound = new LegacySoundStyle(2, 38, Terraria.Audio.SoundType.Sound);
                    }
                    if (sound != null)
                    {
                        Main.PlaySound(sound, footPos);
                    }
                }
                else
                {
                    // Woosh woosh
                    Main.PlaySound(SoundID.Item, footPos);
                }

                // Hit the guy
                if ((projectile.damage >= 15 && Main.time % 2 == 0) || (projectile.damage < 15 && Main.time % 4 == 0))
                {
                    Vector2 pos = new Vector2(projectile.position.X + projectile.width / 2 + 20f * projectile.spriteDirection, projectile.position.Y + Main.rand.NextFloat(-16f, 24f) + projectile.height / 2);
                    //Vector2 direction = Vector2.Normalize(Vector2.Subtract(target.position, pos));
                    Vector2 direction = new Vector2(projectile.spriteDirection, 0);
                    Projectile.NewProjectile
                        (
                        pos,
                        Vector2.Multiply(direction, 25f),
                        fistProjectile,
                        projectile.damage,
                        projectile.knockBack,
                        projectile.owner
                        );
                    //target.StrikeNPC(projectile.damage, projectile.knockBack, projectile.spriteDirection);
                }

                // Also animate accordingly
                if (projectile.frame < 8) projectile.frame = 8;
                if (projectile.frameCounter >= 1)
                {
                    if (projectile.frame >= 15)
                    {
                        projectile.frame = 10;
                    }
                    else
                    {
                        projectile.frame++;
                    }
                    projectile.frameCounter = 0;
                }
                else
                {
                    projectile.frameCounter++;
                }
            }
            else if (Vector2.Distance(targetPos, footPos) < aiMaxEngagementDistance)
            {
                // Chasing animation needs to be mutually exclusive to attack rush
                projectile.frame = 7;
            }
            if (Vector2.Distance(targetPos, footPos) < aiMaxEngagementDistance)
            {
                // Chase and follow
                float distance = Vector2.Distance(footPos, targetPos);
                if (distance >= 0.1f)
                {
                    Vector2 direction = Vector2.Normalize(Vector2.Subtract(targetPos, footPos));
                    Vector2 newVelocity = Vector2.Multiply(direction, distance * aiEngageAccel);
                    projectile.velocity = newVelocity;
                }
                else
                {
                    projectile.velocity = Vector2.Multiply(projectile.velocity, 0.5f);
                }
                return true;
            }
            return false;
        }

        private bool AIChaseNear()
        {
            // Preliminaries
            Player player = Main.player[projectile.owner];
            Vector2 centerPos = new Vector2(projectile.position.X + (projectile.width / 2), projectile.position.Y + (projectile.height / 2));
            Vector2 targetPos = new Vector2(player.position.X + player.width / 2 - aiSpacing.X * player.direction, player.position.Y + player.height/2 - aiSpacing.Y);

            // Animate
            int summonAnimDuration = 8;
            int summonAnimFrames = 4;
            if (projectile.frame <= summonAnimFrames - 1)
            {
                // Initial summon
                if (projectile.frameCounter >= summonAnimDuration/summonAnimFrames)
                {
                    projectile.frame++;
                    projectile.frameCounter = 0;
                    Vector2 offsetPos = new Vector2(Main.rand.NextFloat(-projectile.width, projectile.width), Main.rand.NextFloat(-projectile.height / 2, projectile.height / 2));
                    Dust.NewDust(Vector2.Add(centerPos, offsetPos), 32, 32, mod.DustType("DustMenacing"), 0f, 1f);
                }
                projectile.frameCounter++;
            }
            else
            {
                // Constant idle animations
                if (projectile.frameCounter >= 15)
                {
                    if (projectile.frame >= 6)
                    {
                        projectile.frame = 4;
                    }
                    else
                    {
                        projectile.frame++;
                    }
                    projectile.frameCounter = 0;
                }
                projectile.frameCounter++;
            }

            // Chase the player
            if (player.position.X < centerPos.X)
            {
                projectile.spriteDirection = -1;
            }
            else
            {
                projectile.spriteDirection = 1;
            }
            float distance = Vector2.Distance(centerPos, targetPos);
            if (distance >= 0.1f )
            {
                Vector2 direction = Vector2.Normalize(Vector2.Subtract(targetPos, centerPos));
                Vector2 newVelocity = Vector2.Multiply(direction, distance * aiIdleAccel);
                projectile.velocity = newVelocity;
            }
            else
            {
                projectile.velocity = Vector2.Multiply(projectile.velocity, 0.5f);
            }
            return true;
        }
    }
}
