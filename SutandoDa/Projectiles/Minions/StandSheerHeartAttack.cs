﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Minions
{
    public class StandSheerHeartAttack : Stand
    {
        public override void SetStaticDefaults()
        {
            // Frames:
            // 1-2: Moving animations
            Main.projFrames[projectile.type] = 2;
            Main.projPet[projectile.type] = true;
        }

        public override void SetDefaults()
        {
            projectile.netImportant = true;
            projectile.width = 16;
            projectile.height = 16;
            projectile.friendly = true;
            projectile.minion = true;
            projectile.minionSlots = 0;
            projectile.penetrate = -1;
            projectile.timeLeft = 60;
            projectile.tileCollide = false;
            projectile.ignoreWater = true;
            projectile.aiStyle = 0;
        }

        public override void Behavior()
        {
            const float velocity = 3f;
            const float opacityDistance = 64f;
            const float opacityMinimum = 0.05f;

            projectile.spriteDirection = projectile.direction;

            // Set framecounter to higher than 300 to set a cooldown
            if (projectile.frameCounter < 300)
            {
                // Initial summon velocity and deadstop protection
                if (Math.Abs(projectile.velocity.X) < velocity)
                {
                    projectile.velocity += new Vector2(projectile.direction * velocity, 0);
                }
                // Horizontal speed cap
                if (Math.Abs(projectile.velocity.X) > velocity)
                {
                    projectile.velocity.X = projectile.direction * velocity;
                }
                // Gravity
                projectile.velocity += new Vector2(0, 0.2f);
                if (projectile.velocity.Y > 16f)
                {
                    projectile.velocity.Y = 16f;
                }

                Vector2 collision = Collide();
                if (collision != projectile.velocity)
                {
                    if (Math.Abs(projectile.velocity.X - collision.X) > 0.125)
                    {
                        projectile.direction *= -1;
                    }
                    projectile.velocity = collision;
                    if (projectile.velocity.X == 0)
                    {
                        projectile.velocity.X = projectile.direction * velocity;
                    }
                    projectile.velocity.X = Math.Abs(projectile.velocity.X) * projectile.direction;
                }

                // Animate
                if (projectile.frameCounter % 6 == 0)
                {
                    switch (projectile.frame)
                    {
                        case 1:
                            projectile.frame = 0;
                            break;
                        default:
                            projectile.frame++;
                            break;
                    }
                }

                // KOCHI WO MIRO
                TryDetonate();
            }
            else
            {
                // Do cooldown things
                projectile.frameCounter--;
                Dust.NewDust(projectile.position, projectile.width, projectile.height, mod.DustType("DustKillerQueenEvil"));
                // Ensure we're presenting this cooldown info to the user
                if (!Main.player[projectile.owner].buffType.Contains(mod.BuffType("DebuffSHACooldown")))
                {
                    Main.player[projectile.owner].AddBuff(mod.BuffType("DebuffSHACooldown"), projectile.frameCounter - 300);
                }
                // Also gravity
                projectile.velocity += new Vector2(0, 0.2f);
                projectile.velocity.X = 0;
                projectile.velocity = Collide();
            }

            // For other teams, SHA will camoflauge with distance
            if (Main.player[projectile.owner].team != Main.player[Main.myPlayer].team)
            {
                float bestDistance = Vector2.Distance(Main.player[projectile.owner].position, projectile.position);
                foreach (Player p in Main.player)
                {
                    float newDistance = Vector2.Distance(p.position, projectile.position);
                    if (newDistance < bestDistance)
                    {
                        bestDistance = newDistance;
                    }
                }
                float opacity = 1 / (bestDistance / opacityDistance);
                projectile.Opacity = opacity;
                if (projectile.Opacity < opacityMinimum)
                {
                    projectile.Opacity = opacityMinimum;
                }
            }
            else
            {
                projectile.Opacity = 1f;
            }
            return;
        }

        private Vector2 Collide()
        {
            // Determine collision
            Vector4 slopeCollision = Collision.SlopeCollision(projectile.position, projectile.velocity, projectile.width, projectile.height);
            projectile.position = slopeCollision.XY();
            projectile.velocity = slopeCollision.ZW();
            float stepSpeed = 5;
            //TODO: Use gfxOffY to actually offset the sprite position
            float gfxOffY = 0;
            Collision.StepUp(ref projectile.position, ref projectile.velocity, projectile.width, projectile.height, ref stepSpeed, ref gfxOffY);
            Vector2 collision = Collision.TileCollision(projectile.position, projectile.velocity, projectile.width, projectile.height);
            return collision;
        }

        public bool TryDetonate()
        {
            float triggerDistance = 96f;
            foreach (Player p in Main.player)
            {
                if (Vector2.Distance(p.position, projectile.position) <= triggerDistance && p.whoAmI != projectile.owner && p.team != Main.player[projectile.owner].team && p.pvpDeath)
                {
                    Detonate();
                    return true;
                }
            }
            foreach (NPC n in Main.npc)
            {
                if (Vector2.Distance(n.position, projectile.position) <= triggerDistance && !n.immortal && !n.friendly && n.life > 0)
                {
                    Detonate();
                    return true;
                }
            }
            return false;
        }

        public void Detonate()
        {
            if (Main.myPlayer == projectile.owner)
            {
                Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenDeploy"), projectile.position);
                Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/KillerQueenAbilityActivate"), projectile.position);
            }
            Projectile.NewProjectile(projectile.position, new Vector2(0, 0), mod.ProjectileType("ExplosionKillerQueen"), 100, 16, projectile.owner);
            projectile.frameCounter = 5 * 60 + 300;
        }

        public override void CheckActive()
        {
            Player player = Main.player[projectile.owner];
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (sPlayer.sheerHeartAttackDeploy)
            {
                projectile.timeLeft = 2;
            }
        }
    }
}
