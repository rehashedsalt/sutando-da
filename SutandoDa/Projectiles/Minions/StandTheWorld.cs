﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Minions
{
    public class StandTheWorld : StandTemplatePunchGhost
    {
        public override void SetStaticDefaults()
        {
            // Frames:
            // 0-3: Summoning
            // 4-6: Idle
            // 7: Chasing
            // 8-9: MUDA wind-up
            // 10-15: MUDAMUDAMUDA
            Main.projFrames[projectile.type] = 16;
            Main.projPet[projectile.type] = true;

            ProjectileID.Sets.MinionSacrificable[projectile.type] = true;
            ProjectileID.Sets.MinionTargettingFeature[projectile.type] = true;

            timeStopImmune = true;
        }

        public override void SetDefaults()
        {
            projectile.netImportant = true;
            projectile.width = 64;
            projectile.height = 48;
            projectile.friendly = true;
            projectile.minion = true;
            projectile.minionSlots = 0;
            projectile.penetrate = -1;
            projectile.timeLeft = 60;
            projectile.tileCollide = false;
            projectile.ignoreWater = true;
            projectile.aiStyle = 0;
            fistProjectile = mod.ProjectileType("TheWorldFist");
        }
    }
}
