﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Minions
{
    public class StandKillerQueen : Stand
    {
        public override void SetStaticDefaults()
        {
            // Frames:
            // 1: Skull-faced summoning
            // 2: Purple-faced summoning
            // 2: Idle/chasing
            Main.projFrames[projectile.type] = 16;
            Main.projPet[projectile.type] = true;

            ProjectileID.Sets.MinionSacrificable[projectile.type] = true;
        }

        public override void SetDefaults()
        {
            projectile.netImportant = true;
            projectile.width = 64;
            projectile.height = 48;
            projectile.friendly = true;
            projectile.minion = true;
            projectile.minionSlots = 0;
            projectile.penetrate = -1;
            projectile.timeLeft = 60;
            projectile.tileCollide = false;
            projectile.ignoreWater = true;
            projectile.aiStyle = 0;
        }

        public override void CheckActive()
        {
            Player player = Main.player[projectile.owner];
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            if (player.dead)
            {
                sPlayer.standExists = false;
                projectile.timeLeft = 0;
            }
            if (sPlayer.standExists)
            {
                projectile.timeLeft = 2;
            }
        }

        public override void Behavior()
        {
            Player player = Main.player[projectile.owner];
            SutandoDaPlayer sPlayer = player.GetModPlayer<SutandoDaPlayer>();
            
            if (!player.buffType.Contains(mod.BuffType("DebuffBombCooldown")))
            {
                if (sPlayer.killerQueenBomb == null)
                {
                    AttemptBombPlayer();
                    AttemptBombNPC();
                }
            }
            AIChase();

            // Boilerplate
            projectile.spriteDirection = projectile.direction;
        }

        public void AttemptBombPlayer()
        {
            Player target = null;
            foreach (Player p in Main.player)
            {
                if (Vector2.Distance(p.position, projectile.position) <= 128f && !p.immune && p.team != Main.player[projectile.owner].team && p.whoAmI != Main.player[projectile.owner].whoAmI)
                {
                    if (target == null)
                    {
                        target = p;
                        continue;
                    }
                    if (Vector2.Distance(target.position, projectile.position) > Vector2.Distance(p.position, projectile.position))
                    {
                        target = p;
                    }
                }
            }
            if (target != null)
            {
                Main.player[projectile.owner].GetModPlayer<SutandoDaPlayer>().killerQueenBomb = new KillerQueenBomb(mod, Main.player[projectile.owner], target);
            }
        }

        public void AttemptBombNPC()
        {
            NPC target = null;
            foreach (NPC n in Main.npc)
            {
                if ((Vector2.Distance(n.position, projectile.position) <= 128f || (n.boss && Vector2.Distance(n.position, projectile.position) <= 256f)) && !n.immortal && !n.friendly && n.CanBeChasedBy((object)this, false))
                {
                    // Pick the first thing we can get
                    if (target == null)
                    {
                        target = n;
                        continue;
                    }
                    // Then look for better targets
                    if (Vector2.Distance(target.position, projectile.position) > Vector2.Distance(n.position, projectile.position))
                    {
                        target = n;
                    }
                }
            }
            if (target != null)
            {
                Main.player[projectile.owner].GetModPlayer<SutandoDaPlayer>().killerQueenBomb = new KillerQueenBomb(mod, Main.player[projectile.owner], target);
            }
        }

        public void AIChase()
        {
            const float offsetDistance = 24f;
            const int summonOffsetFrames = 60;
            const int summonObscureFadeInFrames = 15;
            const int summonObscureFrames = 30;

            Player player = Main.player[projectile.owner];
            Vector2 targetPos = new Vector2(player.position.X - player.width - 4, player.position.Y - 4);
            Vector2 offsetPos;

            if (projectile.frame == 0)
            {
                offsetPos = new Vector2(projectile.frameCounter / (float)summonOffsetFrames * player.direction * offsetDistance * -1, 0);
            }
            else
            {
                offsetPos = new Vector2(offsetDistance * player.direction * -1, 0);
            }

            targetPos += offsetPos;

            switch (projectile.frame)
            {
                case 0:
                    projectile.frameCounter++;
                    float alpha = (summonOffsetFrames - projectile.frameCounter) / (float)summonOffsetFrames * 255;
                    projectile.alpha = (int)alpha;
                    for (int i = 0; i < projectile.alpha / 32 + 4; i++)
                    {
                        Dust.NewDust(new Vector2(projectile.position.X + 24 - player.width / 4, projectile.position.Y), (int)(player.width * 1.5), player.height, mod.DustType("DustKillerQueenEvil"));
                    }
                    if (projectile.frameCounter >= summonOffsetFrames - summonObscureFadeInFrames)
                    {
                        for (int i = 0; i < projectile.frameCounter - summonOffsetFrames + summonObscureFadeInFrames + 10; i++)
                        {
                            Dust.NewDust(new Vector2(projectile.position.X + 24, projectile.position.Y), player.width, player.width, mod.DustType("DustKillerQueenEvil"));
                        }
                    }
                    if (projectile.frameCounter >= summonOffsetFrames)
                    {
                        projectile.frame++;
                        projectile.frameCounter = 0;
                    }
                    projectile.position = targetPos;
                    projectile.velocity = Vector2.Multiply(player.velocity, 0.5f);
                    break;
                case 1:
                    projectile.frameCounter++;
                    projectile.alpha = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        Dust.NewDust(new Vector2(projectile.position.X + 24, projectile.position.Y), player.width, player.width, mod.DustType("DustKillerQueenEvil"));
                    }
                    if (projectile.frameCounter >= summonObscureFrames)
                    {
                        projectile.frame++;
                        projectile.frameCounter = 0;
                    }
                    projectile.position = targetPos;
                    projectile.velocity = Vector2.Multiply(player.velocity, 0.5f);
                    break;
                default:
                    if (player.GetModPlayer<SutandoDaPlayer>().killerQueenBomb != null)
                    {
                        Dust.NewDust(new Vector2(projectile.position.X + 24, projectile.position.Y), player.width, player.height, mod.DustType("DustKillerQueenEvil"));
                    }
                    float distance = Vector2.Distance(projectile.position, targetPos);
                    if (distance >= 0.1f)
                    {
                        Vector2 direction = Vector2.Normalize(Vector2.Subtract(targetPos, projectile.position));
                        Vector2 newVelocity = Vector2.Multiply(direction, 2 * distance / 3);
                        projectile.velocity = newVelocity;
                    }
                    else
                    {
                        projectile.velocity = Vector2.Multiply(projectile.velocity, 0.5f);
                    }
                    break;
            }
            projectile.direction = player.direction;
        }
    }
}
