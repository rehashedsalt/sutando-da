﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles
{
    public class StarPlatinumFist : ModProjectile
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Star Platinum's Fist");
        }

        public override void SetDefaults()
        {
            projectile.width = 12;
            projectile.height = 12;
            projectile.aiStyle = 1;
            projectile.friendly = true;
            projectile.minion = true;
            projectile.ignoreWater = true;
            projectile.tileCollide = false;
            projectile.timeLeft = 6;
        }

        public override void OnHitNPC(NPC target, int damage, float knockback, bool crit)
        {
            if (Main.rand.Next(2) == 0)
            {
                Dust.NewDust(projectile.position, 64, 64, mod.DustType("DustHitHardDark"));
            }
            base.OnHitNPC(target, damage, knockback, crit);
        }

        public override void AI()
        {
            projectile.spriteDirection = projectile.direction;
            projectile.rotation = projectile.velocity.ToRotation();
        }
    }
}
