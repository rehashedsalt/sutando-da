﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles
{
    public class ExplosionKillerQueen : ModProjectile
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Killer Queen Explosion");
        }

        public override void SetDefaults()
        {
            projectile.width = 256;
            projectile.height = 256;
            projectile.aiStyle = 0;
            projectile.friendly = true;
            projectile.minion = true;
            projectile.ignoreWater = true;
            projectile.maxPenetrate = -1;
            projectile.penetrate = -1;
            projectile.tileCollide = false;
            projectile.timeLeft = 20;
        }

        public override void AI()
        {
            float maxSize = 1f;

            projectile.scale = (float)Math.Sqrt(20 - projectile.timeLeft) / 1.5f;
            if (projectile.scale > maxSize)
            {
                projectile.scale = maxSize;
            }
            if (projectile.timeLeft <= 15)
            {
                projectile.alpha = 255 - ((255 / 15) * projectile.timeLeft);
            }
        }
    }
}
