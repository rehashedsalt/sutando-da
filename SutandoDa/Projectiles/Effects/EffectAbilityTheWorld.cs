﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Effects
{
    class EffectAbilityTheWorld : Effect
    {
        public override void SetStaticDefaults()
        {
            Main.projFrames[projectile.type] = 4;
            base.SetStaticDefaults();
        }

        public override void SetDefaults()
        {
            projectile.width = 208;
            projectile.height = 144;
            base.SetDefaults();
        }

        public override void AI()
        {
            projectile.frameCounter++;
            if (projectile.frameCounter >= 4 && projectile.frame != 3)
            {
                // Animate him for the first few frames
                projectile.frameCounter = 0;
                projectile.frame = (projectile.frame + 1) % 4;
            }
            else if (projectile.frameCounter > 4 && projectile.frameCounter < 32)
            {
                // Then shake him around on his last frame
                // Should coincide with the climax of the "Time Stop" SFX
                int intensity = 16 - (projectile.frameCounter / 2);
                if (projectile.frameCounter % 2 == 0)
                {
                    projectile.position.Y += intensity;
                }
                else
                {
                    projectile.position.Y -= intensity;
                }
            }
            else if (projectile.frameCounter >= 48 && projectile.frameCounter < 64)
            {
                // Fade him out during his last few frames
                projectile.Opacity = 1.0f - (((float)projectile.frameCounter - 48f) / 16f);
            }
            else if (projectile.frameCounter >=64)
            {
                // Then remove the sprite
                projectile.timeLeft = 0;
            }
        }
    }
}
