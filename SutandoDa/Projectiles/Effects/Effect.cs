﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Effects
{
    /// <summary>
    /// A template for a simple animation projectile
    /// </summary>
    public abstract class Effect : ModProjectile
    {
        public override void SetDefaults()
        {
            projectile.friendly = true;
            projectile.minion = true;
            projectile.penetrate = 1;
            projectile.timeLeft = 300;
            projectile.tileCollide = false;
            projectile.ignoreWater = true;
            projectile.damage = 0;
            projectile.aiStyle = 0;
        }
    }
}
