﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace SutandoDa.Projectiles.Effects
{
    public class EffectTimeStop : Effect
    {
        public override void SetDefaults()
        {
            projectile.width = 512;
            projectile.height = 512;
            projectile.light = 1f;
            base.SetDefaults();
        }

        public override void AI()
        {
            int endPoint = 90;
            float multiplier = 1.2f;
            Vector3 color;
            switch (Main.rand.Next(2))
            {
                case 0:
                    color = new Vector3(1f, 0.8f, 1f);
                    break;
                case 1:
                    color = new Vector3(0.8f, 1f, 1f);
                    break;
                case 2:
                    color = new Vector3(1f, 1f, 0.8f);
                    break;
                default:
                    color = new Vector3(1f, 1f, 1f);
                    break;
            }

            Vector2 centerPos = new Vector2(projectile.position.X + projectile.width / 2, projectile.position.Y + projectile.width / 2);

            projectile.frameCounter++;
            if (projectile.frameCounter < endPoint / 3)
            {
                projectile.scale *= multiplier;
            }
            else
            {
                projectile.scale /= multiplier;
            }
            projectile.Opacity = 1 / projectile.scale;
            Lighting.AddLight(centerPos, color);
            if (projectile.frameCounter >= endPoint)
            {
                projectile.Kill();
            }
        }
    }
}
