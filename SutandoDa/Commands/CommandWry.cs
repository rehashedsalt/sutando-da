﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Commands
{
    class CommandWry : ModCommand
    {
        public override CommandType Type { get { return CommandType.World; } }
        public override string Command { get { return "wry"; } }
        public override string Usage { get { return "/wry"; } }
        public override string Description { get { return "WRYYYYYYYYYYYY"; } }

        public override void Action(CommandCaller caller, string input, string[] args)
        {
            Player player = caller.Player;
            if (player == null) return;
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/EasterEgg/EasterEggSoundWry"), player.position);
        }
    }
}
