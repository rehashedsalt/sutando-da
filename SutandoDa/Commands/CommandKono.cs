﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace SutandoDa.Commands
{
    class CommandKono : ModCommand
    {
        public override CommandType Type { get { return CommandType.World; } }
        public override string Command { get { return "kono"; } }
        public override string Usage { get { return "/kono"; } }
        public override string Description { get { return "IT WAS ME"; } }

        public override void Action(CommandCaller caller, string input, string[] args)
        {
            Player player = caller.Player;
            if (player == null) return;
            Main.PlaySound(mod.GetLegacySoundSlot(SoundType.Custom, "Sounds/EasterEgg/EasterEggSoundKono"), player.position);
            // Fun fact: lowercase strings, when converted ToLower(), can be different depending on locale
            // No clue if that's the case here, but I don't want to take any chances
            if (player.name.ToLower() != "dio".ToLower())
            {
                PlayerDeathReason reason = PlayerDeathReason.ByCustomReason("You thought it was Dio, BUT IT WAS ME, " + player.name.ToUpper() + "!");
                player.Hurt(reason, int.MaxValue, 0, false, false, true);
            }
        }
    }
}
